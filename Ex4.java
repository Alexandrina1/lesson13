package lesson13;

public class Ex4 {
    public static void traversArrayBidimensional(int [][] arr1)
    {
        for (int i = 0; i<arr1.length; i++)
        {
            for (int j=0; j<arr1.length; j++){
                System.out.print(arr1[i][j] + " ");
            }
            System.out.println(" ");
        }
    }
    public static void main(String[] arg)
    {
        Ex4 ex4 = new Ex4();
        ex4.traversArrayBidimensional(new int[][]{{5,6,7},{3,6,8},{7,6,8}});

    }
}
